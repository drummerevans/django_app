This directory contains features to display a map of Devon.

## MyMap.html

This displays a map over Exeter, Devon. 
To run use

```bash
start MyMap.html
```

USEFUL GitLab info on Branches:

To create a new branch use:

```bash
git branch <new_branch>
```

Change branch:

```bash
git checkout <new_branch>
```

Then make changes...

```bash
git add --all
git commit -m "MY MESSAGE HERE"
git push --set-upstream origin <new_branch>
```

List all branches:

```bash
git branch --list
```

Merging into 'main' branch:

```bash
git checkout main
git merge <new_branch>
git push origin main
```